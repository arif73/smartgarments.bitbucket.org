<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.admin.master');
});


Route::get('/dashboard', function () {
    return view('layouts.admin.dashboard');
});


Route::get('/profileView', [
    'uses'  =>  'ProfileController@profileView',
    'as'    =>  'profileView'
]);



Route::get('/addProfile', [
'uses'  =>  'ProfileController@addProfile',
    'as'    =>  'add_profile'
]);


Route::get('/editProfile', [
'uses'  =>  'ProfileController@editProfile',
    'as'    =>  'edit_profile'
]);


Route::get('/addAppoinment', [
'uses'  =>  'AppoinmentController@index',
    'as'    =>  'add_appointment'
]);
