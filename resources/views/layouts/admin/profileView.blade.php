@extends('layouts.admin.master')
@section('body')
    <div class="col-lg-12">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Tabs</h2>
                <div class="actions panel_actions pull-right">
                    <i class="box_toggle fa fa-chevron-down"></i>
                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                    <i class="box_close fa fa-times"></i>
                </div>
            </header>
            <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">



                        <!-- Horizontal - start -->
                        <div class="row">
                            <div class="col-md-12">

                                <h4>Profile View</h4>

                                <ul class="nav nav-tabs primary">
                                    <li class="active">
                                        <a href="#home-1" data-toggle="tab">
                                            <i class="fa fa-user"></i> Basic Info
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#profile-1" data-toggle="tab">
                                            <i class="fa fa-user"></i> Details Info
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#messages-1" data-toggle="tab">
                                            <i class="fa fa-user"></i> Fitness Info
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#settings-1" data-toggle="tab">
                                            <i class="fa fa-user"></i> Nominee Info
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content primary">
                                    <div class="tab-pane fade in active" id="home-1">

                                        <div>

                                            <div class="content-body">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-9 col-xs-10">

                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1">Name</label>
                                                            <span class="desc">e.g. "your name"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="ARIFA" readonly >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1"> Father's/Husband's Name</label>
                                                            <span class="desc">e.g. "your father's name"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="ATIKUR" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1">Mother's Name</label>
                                                            <span class="desc">e.g. "your mother's name"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="NILA" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-6">Present Address</label>
                                                            <span class="desc">e.g. "Enter your current address here"</span>
                                                            <div class="controls">
                                                                <textarea class="form-control" cols="5" id="field-6" readonly>ggsdgsdgsdzgsdgsdg</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-6">Permanent Address</label>
                                                            <span class="desc">e.g. "Enter your permanent address here"</span>
                                                            <div class="controls">
                                                                <textarea class="form-control" cols="5" id="field-6" readonly>nxnhsxfjsxsj</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1"> Designation</label>
                                                            <span class="desc">e.g. "your designation"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="SEWING" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1"> Card No</label>
                                                            <span class="desc">e.g. "your card number"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="154" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1"> Grade</label>
                                                            <span class="desc">e.g. "your grade here"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="4" readonly >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1"> Section</label>
                                                            <span class="desc">e.g. "your  section here"</span>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" id="field-1" value="4" readonly>
                                                            </div>
                                                        </div>
                                                    </div></div></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="profile-1">
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">National ID copy</label>
                                            <span class="desc">e.g. "NID.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Interview report copy,</label>
                                            <span class="desc">e.g. "file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10"  value="c:/passwords.txt">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Operator’s duty copy</label>
                                            <span class="desc">e.g. "file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10"  value="c:/passwords.txt">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="messages-1">

                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Upload File For Fitness Certificate</label>
                                            <span class="desc">e.g. "fitness_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Upload File For Age Certificate</label>
                                            <span class="desc">e.g. "age_certificate_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="settings-1">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1"> Nominee Name</label>
                                            <span class="desc">e.g. "your  name here"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-1"  value="Sofiq" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Upload Image</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield5">Age</label>
                                            <span class="desc">e.g. "your age"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="formfield5" name="formfield5" value="22" readonly >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="field-1"> Relation With The Person</label>
                                            <span class="desc">e.g. "your  relationship here"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-1" value="Father" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="field-1"> Submit Book</label>
                                            <span class="desc">e.g. "your  book  here"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-1" value="Service Book" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="field-1"> Percentage</label>
                                            <span class="desc">e.g. "your  info here"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-1" value="50" readonly >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Tofsil 41 form</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Address File</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10"value="c:/passwords.txt">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Passport/NID</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Birth Certificate</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10"value="c:/passwords.txt" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="formfield10">Driving License</label>
                                            <span class="desc">e.g. "image_file.jpg"</span>
                                            <div class="controls">
                                                <input type="file" class="form-control" id="formfield10" name="formfield10" value="c:/passwords.txt">
                                            </div>
                                        </div>




                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"><br></div>



                        </div>

                        <!-- Horizontal - end -->

                        <div class="clearfix hidden-md hidden-lg"><br></div>
                        <!-- Vertical - start -->




                    </div>
                </div>
            </div>
        </section></div>


@endsection