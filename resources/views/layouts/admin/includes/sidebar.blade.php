




    <!-- SIDEBAR - START -->
    <div class="page-sidebar ">

        <!-- MAIN MENU - START -->
        <div class="page-sidebar-wrapper" id="main-menu-wrapper">

            <!-- USER INFO - START -->
            <div class="profile-info row">

                <div class="profile-image col-md-4 col-sm-4 col-xs-4">
                    <a href="">
                        <img src="{{ asset('/') }}data/profile/profile.png" class="img-responsive img-circle">
                    </a>
                </div>

                <div class="profile-details col-md-8 col-sm-8 col-xs-8">

                    <h3>
                        <a href="">Jason Bourne</a>

                        <!-- Available statuses: online, idle, busy, away and offline -->
                        <span class="profile-status online"></span>
                    </h3>

                    <p class="profile-title">Web Developer</p>

                </div>

            </div>
            <!-- USER INFO - END -->



            <ul class='wraplist'>

                <li class="">
                    <a href="{{ route('profileView') }}">
                        <i class="fa fa-dashboard"></i>
                        <span class="title">Profile View Page</span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-suitcase"></i>
                        <span class="title">Profile</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" >
                        <li>
                            <a class="" href="{{ route('add_profile') }}" >Add Profile</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('edit_profile') }}" >Edit Profile</a>
                        </li>

                    </ul>
                </li>

                <li class="">
                    <a href="{{ route('add_appointment') }}">
                        <i class="fa fa-dashboard"></i>
                        <span class="title">Appointment LetterGeneration  Page</span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- MAIN MENU - END -->



        <div class="project-info">

            <div class="block1">
                <div class="data">
                    <span class='title'>New&nbsp;Orders</span>
                    <span class='total'>2,345</span>
                </div>
                <div class="graph">
                    <span class="sidebar_orders">...</span>
                </div>
            </div>

            <div class="block2">
                <div class="data">
                    <span class='title'>Visitors</span>
                    <span class='total'>345</span>
                </div>
                <div class="graph">
                    <span class="sidebar_visitors">...</span>
                </div>
            </div>

        </div>



    </div>
    <!--  SIDEBAR - END -->







