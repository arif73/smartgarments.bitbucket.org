<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public  function profileView(){
        return view('layouts.admin.profileView');
    }

    public  function addProfile(){
        return view('layouts.admin.add_profile');
    }

    public  function editProfile(){
        return view('layouts.admin.edit_profile');
    }




}
